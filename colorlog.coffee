
global.FG = {}
global.BG = {}
for name, i in ['BLACK', 'MAROON', 'GREEN', 'GOLD', 'NAVY', 'PURPLE', 'TEAL', 'SILVER']
	global.FG[name] = "\x1B[0;3#{i}m"
	global.BG[name] = "\x1B[0;4#{i}m"
for name, i in ['GRAY', 'RED', 'LIME', 'YELLOW', 'BLUE', 'MAGENTA', 'CYAN', 'WHITE']
	global.FG[name] = "\x1B[1;3#{i}m"
	global.BG[name] =  "\x1B[10#{i}m"
global.RESET     = "\x1B[0m"
global.UNDERLINE = "\x1B[4m"
global.NL        = "\n"
global.log = (color, msg) -> console.log color+msg+RESET
